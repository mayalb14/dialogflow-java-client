/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bl;

import ai.api.AIConfiguration;
import ai.api.AIDataService;
import ai.api.AIServiceException;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;

/**
 *
 * @author Alexander Mayer
 */
public class DialogflowManager
{
    private AIConfiguration configuration;
    private AIDataService dataService;

    /**
     *
     * @param token Dialogflow API V1 token
     */
    public DialogflowManager(String token)
    {
        configuration = new AIConfiguration(token);
        dataService = new AIDataService(configuration);
    }

    public String getHTMLResponse(String text)
    {
        try
        {
            AIRequest request = new AIRequest(text);
            AIResponse response = dataService.request(request);
            
            if(response.getStatus().getCode() == 200)
            {
                return response.getResult().getFulfillment().getSpeech();
            }
            else
            {
                return response.getStatus().getErrorDetails();
            }
        }
        catch(AIServiceException ex)
        {
            return ex.getMessage();
        }
    }
}
